import React from 'react';
import ru from '../locale/ru';

const FormattedMessage = props => {
    return <span>{ru[props.id]}</span>;
};

export default FormattedMessage;
