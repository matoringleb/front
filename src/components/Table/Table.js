import React from 'react';
import FormattedMessage from '../../service/FormattedMessage';
import PropTypes, { shape } from 'prop-types';
import styled, { css } from 'styled-components/macro';

//TODO add 'filled'
const StyledTable = styled.table`
    ${({ round }) =>
        round &&
        `
    border-color: grey;
    border-style: solid;
    border-width: 3px;
    border-radius: 3px;
  `}
    ${({ homeTeam, oppositeTeam }) =>
        homeTeam &&
        !oppositeTeam &&
        css`
            border-color: ${props => props.theme.homeTeam};
        `};
    ${({ homeTeam, oppositeTeam }) =>
        !homeTeam &&
        oppositeTeam &&
        css`
            border-color: ${props => props.theme.oppositeTeam};
        `};
`;

const StyledCell = styled.tr`
    ${({ cellColored }) =>
        cellColored &&
        `
    &:nth-child(2n + 1) {
    background-color: grey;
    }
  `}
    ${({ homeTeam, oppositeTeam }) =>
        homeTeam &&
        !oppositeTeam &&
        css`
            &:nth-child(2n + 1) {
                background-color: ${props => props.theme.homeTeam};
            }
        `};
    ${({ homeTeam, oppositeTeam }) =>
        !homeTeam &&
        oppositeTeam &&
        css`
            &:nth-child(2n + 1) {
                background-color: ${props => props.theme.oppositeTeam};
            }
        `};
`;

const dispatchColumns = type => {
    if (type !== 'pp' && type !== 'pk' && type !== 'summary') {
        return [];
    }
    return [
        {
            header: <FormattedMessage id="report.gameTotal.player" />,
            render: e => e.playerId
        },
        {
            header: <FormattedMessage id="report.gameTotal.toi" />,
            render: e => e[type].toi
        },
        {
            header: <FormattedMessage id="report.gameTotal.shifts" />,
            render: e => e[type].shifts
        },
        {
            header: <FormattedMessage id="report.gameTotal.avgShift" />,
            render: e => e[type].avgShift
        },
        {
            header: <FormattedMessage id="report.gameTotal.scf" />,
            render: e => e[type].scf
        },
        {
            header: <FormattedMessage id="report.gameTotal.sca" />,
            render: e => e[type].sca
        },
        {
            header: <FormattedMessage id="report.gameTotal.gf" />,
            render: e => e[type].gf
        },
        {
            header: <FormattedMessage id="report.gameTotal.ga" />,
            render: e => e[type].ga
        },
        {
            header: <FormattedMessage id="report.gameTotal.blkShots" />,
            render: e => e[type].blkShots
        }
    ];
};

function renderTable(columns) {
    return columns.map((column, index) => <td key={index}>{column.header}</td>);
}

function Table(props) {
    const { type, data, round, cellColored, homeTeam, oppositeTeam } = props;
    return (
        <StyledTable homeTeam={homeTeam} oppositeTeam={oppositeTeam} round={round} cellColored={cellColored}>
            <thead>
                <tr>{renderTable(dispatchColumns(props.type))}</tr>
            </thead>
            <tbody>
                {data.skaters.map((row, index) => (
                    <StyledCell cellColored={cellColored} homeTeam={homeTeam} oppositeTeam={oppositeTeam} key={index}>
                        {dispatchColumns(type).map((column, index) => (
                            <td key={index}>{column.render(row)}</td>
                        ))}
                    </StyledCell>
                ))}
            </tbody>
        </StyledTable>
    );
}

const skatersType = PropTypes.shape({
    toi: PropTypes.number.isRequired,
    shifts: PropTypes.number.isRequired,
    avgShift: PropTypes.number.isRequired,
    scf: PropTypes.number.isRequired,
    sca: PropTypes.number.isRequired,
    gf: PropTypes.number.isRequired,
    ga: PropTypes.number.isRequired,
    blkShots: PropTypes.number.isRequired
});

Table.propTypes = {
    type: PropTypes.oneOf(['pp', 'pk', 'summary']),
    data: PropTypes.shape({
        skaters: PropTypes.shape({
            pp: skatersType,
            pk: skatersType,
            summary: skatersType
        })
    })
};
export default Table;
