Table example:

```js
import data from '../../fakeData/data';
import theme from '../../theme/theme.js'
import {ThemeProvider} from 'styled-components';

    <ThemeProvider theme={theme} >
      <div style={{display:'flex', justifyContent: "space-around"}}>
        <Table data={data} type={'pp'} homeTeam round/>
        <Table data={data} type={'pp'} oppositeTeam round/>
      </div>
    </ThemeProvider>

```

