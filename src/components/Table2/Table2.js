import React from 'react';
import FormattedMessage from '../../service/FormattedMessage';
import PropTypes, { shape } from 'prop-types';
import styled, { css } from 'styled-components/macro';
import Page from '../Page/Page';

const Wrapper = styled.div`
    display: grid;
    grid-template-columns: 0.07fr 0.07fr 0.07fr 0.195fr 0.19fr 0.195fr 0.07fr 0.07fr 0.07fr;
    grid-template-rows: 3fr;
    grid-column-gap: 0px;
    grid-row-gap: 0px;
    height: ${props => props.height};
`;

//TODO add 'filled'
const StyledTable = styled.table`
    ${({ round }) =>
        round &&
        `
            width: 100%;
    border-color: grey;
    border-style: solid;
    border-width: 3px;
    border-radius: 3px;
  `}
    ${({ homeTeam, oppositeTeam }) =>
        homeTeam &&
        !oppositeTeam &&
        css`
            border-color: ${props => props.theme.homeTeam};
        `};
    ${({ homeTeam, oppositeTeam }) =>
        !homeTeam &&
        oppositeTeam &&
        css`
            border-color: ${props => props.theme.oppositeTeam};
        `};
`;

const StyledCell = styled.tr`
    ${({ cellColored }) =>
        cellColored &&
        `
    &:nth-child(2n + 1) {
    background-color: grey;
    }
  `}
    ${({ homeTeam, oppositeTeam }) =>
        homeTeam &&
        !oppositeTeam &&
        css`
            &:nth-child(2n + 1) {
                background-color: ${props => props.theme.homeTeam};
            }
        `};
    ${({ homeTeam, oppositeTeam }) =>
        !homeTeam &&
        oppositeTeam &&
        css`
            &:nth-child(2n + 1) {
                background-color: ${props => props.theme.oppositeTeam};
            }
        `};
`;

const dispatchColumns = type => {
    if (type !== 'pp' && type !== 'pk' && type !== 'summary') {
        return [];
    }
    return [
        {
            header: <FormattedMessage id="report.gameTotal.pk" />,
            render: e => e.playerId
        },
        {
            header: <FormattedMessage id="report.gameTotal.pp" />,
            render: e => e[type].toi
        },
        {
            header: <FormattedMessage id="report.gameTotal.even" />,
            render: e => e[type].shifts
        },
        {
            header: <FormattedMessage id="report.gameTotal.total" />,
            render: e => e[type].avgShift
        },
        {
            header: <FormattedMessage id="report.gameTotal.section" />,
            render: e => e[type].scf
        },
        {
            header: <FormattedMessage id="report.gameTotal.total" />,
            render: e => e[type].sca
        },
        {
            header: <FormattedMessage id="report.gameTotal.even" />,
            render: e => e[type].gf
        },
        {
            header: <FormattedMessage id="report.gameTotal.pp" />,
            render: e => e[type].ga
        }
    ];
};

function renderTable(columns) {
    return columns.map((column, index) => <td key={index}>{column.header}</td>);
}

function Table2(props) {
    const { type, data, round, cellColored, homeTeam, oppositeTeam } = props;
    console.log('Current theme: ', props.theme);
    return (
        <>
            <StyledTable homeTeam={homeTeam} oppositeTeam={oppositeTeam} round={round} cellColored={cellColored}>
                {/*            <thead>

                <tr>{renderTable(dispatchColumns(props.type))}</tr>

            </thead>*/}
                <tbody>
                    {data.skaters.map((row, index) => (
                        <StyledCell
                            cellColored={cellColored}
                            homeTeam={homeTeam}
                            oppositeTeam={oppositeTeam}
                            key={index}
                        >
                            <Wrapper>
                                {dispatchColumns(type).map((column, index) => (
                                    <td key={index}>{column.render(row)}</td>
                                ))}{' '}
                            </Wrapper>
                        </StyledCell>
                    ))}
                </tbody>
            </StyledTable>
        </>
    );
}

const skatersType = PropTypes.shape({
    toi: PropTypes.number.isRequired,
    shifts: PropTypes.number.isRequired,
    avgShift: PropTypes.number.isRequired,
    scf: PropTypes.number.isRequired,
    sca: PropTypes.number.isRequired,
    gf: PropTypes.number.isRequired,
    ga: PropTypes.number.isRequired,
    blkShots: PropTypes.number.isRequired
});

Table2.propTypes = {
    type: PropTypes.oneOf(['pp', 'pk', 'summary']),
    data: PropTypes.shape({
        skaters: PropTypes.shape({
            pp: skatersType,
            pk: skatersType,
            summary: skatersType
        })
    })
};
export default Table2;
