Table example:

```js
import data from '../../fakeData/data';
import theme from '../../theme/theme.js'
import {ThemeProvider} from 'styled-components';

    <ThemeProvider theme={theme} >

        <Table2 data={data} type={'pp'} homeTeam round/>

    </ThemeProvider>

```

