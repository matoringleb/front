import React from 'react';
import FormattedMessage from '../../service/FormattedMessage';
import PropTypes, { shape } from 'prop-types';
import styled, { css } from 'styled-components/macro';
import Page from '../Page/Page';

const Wrapper = styled.div`
    display: grid;
    grid-template-columns: 0.21fr 0.195fr 0.19fr 0.195fr 0.21fr;
    grid-template-rows: 3fr 3fr;
    grid-column-gap: 0px;
    grid-row-gap: 0px;
    height: ${props => props.height};
`;
const StyledTeamName = styled.h2`
    margin: 0;
    font-size: 10px;
    align-self: flex-end;
    display: inline-flex;
    justify-content: center;

    ${({ homeTeam, oppositeTeam, teamNameCentered }) =>
        homeTeam &&
        !oppositeTeam &&
        !teamNameCentered &&
        css`
            color: ${props => props.theme.homeTeam};
            grid-area: 1 / 1 / 2 / 2;
        `};
    ${({ homeTeam, oppositeTeam, teamNameCentered }) =>
        homeTeam &&
        !oppositeTeam &&
        teamNameCentered &&
        css`
            color: ${props => props.theme.homeTeam};
            grid-area: 1 / 1 / 3 / 2;
            margin: auto;
        `};

    ${({ homeTeam, oppositeTeam, teamNameCentered }) =>
        !homeTeam &&
        oppositeTeam &&
        !teamNameCentered &&
        css`
            color: ${props => props.theme.oppositeTeam};
            grid-area: 1 / 5 / 2 / 6;
        `};
    ${({ homeTeam, oppositeTeam, teamNameCentered }) =>
        !homeTeam &&
        oppositeTeam &&
        teamNameCentered &&
        css`
            color: ${props => props.theme.oppositeTeam};
            grid-area: 1 / 5 / 3 / 6;
            margin: auto;
        `};
`;

const StyledSection = styled.h1`
    text-align: center;
    grid-area: 1 / 3 / 3 / 4;
    margin: auto;
`;

const StyledLogo = styled.img`
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: 100%;
    height: 100%;
    ${({ homeTeamLogo, oppositeTeamLogo }) =>
        homeTeamLogo &&
        !oppositeTeamLogo &&
        css`
            background-image: url('${props => props.homeTeamLogo}');
            grid-area: 1 / 2 / 3 / 3;
        `};

    ${({ homeTeamLogo, oppositeTeamLogo }) =>
        !homeTeamLogo &&
        oppositeTeamLogo &&
        css`
            background-image: url(${props => props.oppositeTeamLogo});
            grid-area: 1 / 4 / 3 / 5;
        `};
    ${({ containImage }) =>
        containImage &&
        `
          background-size: contain;
         `};
`;

const HeaderOne = props => {
    const {
        teams: {
            homeTeam: { name: homeTeamName, logo: homeTeamLogo },
            oppositeTeam: { name: oppositeTeamName, logo: oppositeTeamLogo }
        }
    } = props.data;

    const { teamNameCentered, containImage, height } = props;

    return (
        <Wrapper height={height}>
            <StyledTeamName homeTeam teamNameCentered={teamNameCentered}>
                {homeTeamName}
            </StyledTeamName>
            <StyledLogo homeTeamLogo={homeTeamLogo} containImage={containImage} />
            <StyledSection>
                <FormattedMessage id="report.period.periodOne" />
            </StyledSection>
            <StyledLogo oppositeTeamLogo={oppositeTeamLogo} containImage={containImage} />
            <StyledTeamName oppositeTeam teamNameCentered={teamNameCentered}>
                {oppositeTeamName}
            </StyledTeamName>
        </Wrapper>
    );
};

export default HeaderOne;
