HeaderOne example:



```js
import data from '../../fakeData/data';
import theme from '../../theme/theme.js'
import {ThemeProvider} from 'styled-components';

    <ThemeProvider theme={theme} >
        <HeaderOne height={'50px'} teamNameCentered data={data} />
    </ThemeProvider>


```




