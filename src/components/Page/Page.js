import React from 'react';
import styled from 'styled-components/macro';

const StyledPage = styled.div`
    margin: 0 5%;
`;

const Page = props => {
    return <StyledPage>{props.children}</StyledPage>;
};

export default Page;
