import React from "react";
import FormattedMessage from "../../service/FormattedMessage";
import styled, { css } from "styled-components/macro";

const StyledHeader = styled.div`
height: 50px;
text-align:center;
    ${({ type }) =>
  type === "total" &&
  type !== "positive" &&
  type !== "negative" &&
  type !== "noValue" && css
    `
            background-color: ${props => props.theme.darkblue};
        `};
 
    ${({ type }) =>
  type !== "total" &&
  type === "positive" &&
  type !== "negative" &&
  type !== "noValue" && css
    `
            background-color: ${props => props.theme.lightGreen};
        `};
    ${({ type }) =>
  type !== "total" &&
  type !== "positive" &&
  type !== "negative" &&
  type === "noValue" && css
  `
            background-color: ${props => props.theme.lightYellow};
        `};
    ${({ type }) =>
  type !== "total" &&
  type !== "positive" &&
  type === "negative" &&
  type !== "noValue" && css
  `
            background-color: ${props => props.theme.lightRed};
        `};
`;

const StyledPercent = styled.h4`
text-align:center;
font-size:2rem;
margin: 0.2rem
`;

const dispatchColumns = () => {
  return [
    {
      header: <FormattedMessage id="report.homeTeamEntries.total"/>,
      id: "total",
      render: e => e.total
    },
    {
      header: <FormattedMessage id="report.homeTeamEntries.positiveValue"/>,
      id: "positive",
      render: e => e.positive,
      percent: p => (p.positive / p.total) * 100
    },
    {
      header: <FormattedMessage id="report.homeTeamEntries.noValue"/>,
      id: "noValue",
      render: e => e.noValue,
      percent: p => (p.noValue / p.total) * 100
    },
    {
      header: <FormattedMessage id="report.homeTeamEntries.negativeValue"/>,
      id: "negative",
      render: e => e.negative,
      percent: p => (p.negative / p.total) * 100
    }
  ];
};

function renderTable(columns) {
  return columns.map((column) => (
    <td key={column.id}>
      <StyledHeader type={column.id} >{column.header}  </StyledHeader>
    </td>

  ));
}

const FourvaluesTable = props => {
  const { data} = props;
  return (
    <table>

      <thead>
      <tr> {renderTable(dispatchColumns())}</tr>
      </thead>
      <tbody>

      <tr>
        {dispatchColumns().map((column, index) => (
          <td><StyledPercent>{column.percent ? Math.round(column.percent(data.teams.homeTeam.entries)) + ' %' : null}</StyledPercent></td>
        ))}
      </tr>
      <tr>
        {dispatchColumns().map((column, index) => (
          <td><StyledHeader type={column.id}>{column.render(data.teams.homeTeam.entries)}</StyledHeader></td>
        ))}
      </tr>
      </tbody>
    </table>
  );
};

export default FourvaluesTable;
