Table example:

```js
import data from '../../fakeData/data';
import theme from '../../theme/theme.js'
import {ThemeProvider} from 'styled-components';

    <ThemeProvider theme={theme} >
        
        <FourvaluesTable data={data}/>

    </ThemeProvider>

```

