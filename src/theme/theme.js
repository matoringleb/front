export default {
    homeTeam: 'red',
    oppositeTeam: 'blue',
    darkblue: '#1f4e78',
    lightGreen: '#e2efda',
    lightYellow: '#fff2cc',
    lightRed: '#ffcccc'
};
